# [docker-buildx](https://github.com/docker/buildx) apt packages

### Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-docker-buildx.asc https://packaging.gitlab.io/docker-buildx/gpg.key
```

### Add repo to apt

```bash
echo "deb [arch=amd64] https://packaging.gitlab.io/docker-buildx docker-buildx main" | sudo tee /etc/apt/sources.list.d/morph027-docker-buildx.list
```
