#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

export DEBIAN_FRONTEND=noninteractive
tag="${CI_COMMIT_TAG:-v0.10.0-1}"
version="${tag%-*}"
iteration="${tag##*-}"
name=docker-buildx
description="Docker CLI plugin for extended build capabilities with BuildKit"
architecture="${ARCH:-amd64}"

apt-get -qq update
apt-get -qqy install \
    curl \
    binutils \
    unzip \
    ruby-dev \
    ruby-ffi

type fpm >/dev/null 2>&1 || (
    gem install fpm
)

tmpdir="$(mktemp -d)"

curl -Lo "${tmpdir}/docker-buildx" "https://github.com/docker/buildx/releases/download/${version}/buildx-${version}.linux-${architecture}"
chmod +x "${tmpdir}/docker-buildx"

fpm \
    --input-type dir \
    --output-type deb \
    --force \
    --name "${name}" \
    --package "${CI_PROJECT_DIR}/${name}_${version//v/}-${iteration}_${architecture}.deb" \
    --deb-dist docker-buildx \
    --architecture "${architecture}" \
    --version "${version//v/}" \
    --iteration "${iteration}" \
    --maintainer "Stefan Heitmüller <stefan.heitmueller@gmx.com>" \
    --url "https://github.com/docker/buildx" \
    --description "${description}" \
    --deb-recommends morph027-keyring \
    --prefix "/usr/lib/docker/cli-plugins" \
    --chdir "${tmpdir}" \
    docker-buildx
