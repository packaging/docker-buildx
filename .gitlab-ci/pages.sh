#!/bin/bash

set -e

dist=docker-buildx

mkdir -p "${CI_PROJECT_DIR}"/public
cp -rv \
  "${CI_PROJECT_DIR}"/.repo/"${dist}"/{pool,dists} \
  "${CI_PROJECT_DIR}"/public/
cp -rv \
  "${CI_PROJECT_DIR}"/.repo/gpg.key \
  "${CI_PROJECT_DIR}"/public/
